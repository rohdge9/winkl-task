import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:winkl_task/widgets/WidgetAlertDialog.dart';

//common function use throughout the app
class HelperFunction {
  static showCommonDialog(BuildContext context,
      {Function positiveButton, Function no, String title, Widget content}) {
    showDialog(
        context: context,
        builder: (context) {
          return WidgetAlertDialog(
              title: title,
              content: content,
              positiveButton: positiveButton,
              no: no);
        });
  }

  static showFlushbarSuccess(BuildContext context, String msg) {
    Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      backgroundColor: Colors.green,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  static showFlushbarError(BuildContext context, String msg) {
    Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      backgroundColor: Colors.red,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}
