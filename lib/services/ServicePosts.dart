import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:winkl_task/helper/Secrete.dart';

class ServicePosts {
  static const fetchLimit = 5;
  final baseUrl = "https://api.winkl.in/";

  Future<dynamic> fetchPosts(int page) async {
    Map<String, String> headerWithToken = {
      'Content-Type': 'application/json',
      'Authorization': 'Token ${Secrete.token}'
    };
    try {
      final response = await http.get(
          baseUrl + "sample_videos?limit=$fetchLimit&offset=$page",
          headers: headerWithToken);
      return jsonDecode(response.body);
    } on SocketException catch (e) {
      debugPrint(e.toString());
      return [];
    } catch (e) {
      debugPrint(e.toString());
      return [];
    }
  }
}
