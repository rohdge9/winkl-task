import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:winkl_task/models/ModelPosts.dart';
import 'package:winkl_task/repositories/RepositoryPosts.dart';

part 'posts_state.dart';

class PostsCubit extends Cubit<PostsState> {
  PostsCubit(this.repositoryPosts) : super(PostsInitial());

  int page = 0;
  final RepositoryPosts repositoryPosts;

  void loadPosts() {
    if (state is PostsLoading) return;

    final currentState = state;

    List<ModelPosts> listOldPosts = [];
    if (currentState is PostsLoaded) {
      listOldPosts = currentState.listPosts;
    }

    emit(PostsLoading(listOldPosts, isFirstFetch: page == 0));

    //when we fetch new posts we just append that new posts to old posts
    repositoryPosts.fetchPosts(page).then((listNewPosts) {
      page += 1;
      final posts = (state as PostsLoading).listOldPosts;
      posts.addAll(listNewPosts);
      emit(PostsLoaded(posts));
    });
  }
}
