part of 'posts_cubit.dart';

@immutable
abstract class PostsState {}

class PostsInitial extends PostsState {}

class PostsLoaded extends PostsState {
  final List<ModelPosts> listPosts;
  PostsLoaded(this.listPosts);
}

class PostsLoading extends PostsState {
  final List<ModelPosts> listOldPosts;
  final bool isFirstFetch;
  PostsLoading(this.listOldPosts, {this.isFirstFetch = false});
}
