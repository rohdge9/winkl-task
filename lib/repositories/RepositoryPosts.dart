import 'package:winkl_task/models/ModelPosts.dart';
import 'package:winkl_task/services/ServicePosts.dart';

class RepositoryPosts {
  final ServicePosts servicePosts;

  RepositoryPosts(this.servicePosts);
  Future<List<ModelPosts>> fetchPosts(int page) async {
    List<ModelPosts> listApiPosts = [];
    //call to service layer for getting posts
    await servicePosts.fetchPosts(page).then((apiResponse) {
      apiResponse['data'].forEach((element) {
        //converting api json response to our modelPost
        ModelPosts modelPosts = ModelPosts.fromJson(element);
        listApiPosts.add(modelPosts);
      });
      return listApiPosts;
    });
    return listApiPosts;
  }
}
