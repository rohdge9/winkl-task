import 'dart:async';
import 'dart:io';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:winkl_task/bloc/cubit/posts_cubit.dart';
import 'package:winkl_task/helper/HelperFunction.dart';
import 'package:winkl_task/models/ModelPosts.dart';
import 'package:winkl_task/modules/hive/TBPost.dart';
import 'package:winkl_task/widgets/WidgetAppBar.dart';
import 'package:winkl_task/widgets/WidgetNoDataAvailable.dart';
import 'package:winkl_task/widgets/WidgetPostDetails.dart';

import '../../main.dart';

class DisplayPost extends StatefulWidget {
  @override
  _DisplayPostState createState() => _DisplayPostState();
}

class _DisplayPostState extends State<DisplayPost>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  final scrollController = ScrollController();
  PageController pageController = PageController();
  var currentPageValue = 0.0;

  @override
  void initState() {
    super.initState();
    //2 tabs will be displayed
    tabController = new TabController(vsync: this, length: 2);

    //added listener to controller to get currentPageIndex
    pageController.addListener(() {
      setState(() {
        currentPageValue = pageController.page;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);

    //calling to cubit
    BlocProvider.of<PostsCubit>(context).loadPosts();
    return WillPopScope(
      onWillPop: exitApp,
      child: Scaffold(
          appBar: WidgetAppBar(title: 'Winkl Task'),
          body: Column(
            children: [
              Container(
                height: 30,
                alignment: Alignment.center,
                margin: EdgeInsets.all(15),
                child: TabBar(
                  isScrollable: false,
                  labelPadding: EdgeInsets.zero,
                  physics: NeverScrollableScrollPhysics(),
                  controller: tabController,
                  unselectedLabelColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(30)),
                  tabs: [
                    Tab(
                      text: "All Posts",
                    ),
                    Tab(
                      text: "Favourite Posts",
                    ),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: tabController,
                  children: [
                    widgetAllPosts(),
                    widgetFavouritePosts(),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Widget widgetFavouritePosts() {
    //used for listening real-time changes from database
    return ValueListenableBuilder(
        valueListenable: Hive.box<TBPost>(databaseName).listenable(),
        builder: (context, box, widget1) {
          Box<TBPost> boxPost = box;
          if (boxPost.isEmpty) {
            return WidgetNoDataAvailable(
              title: 'No favourite posts found.',
            );
          }
          return PageView.builder(
            controller: pageController,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              TBPost tbPost = boxPost.getAt(index);
              ModelPosts modelPosts = new ModelPosts(
                  description: tbPost.description,
                  sources: tbPost.sources,
                  subtitle: tbPost.subtitle,
                  thumb: tbPost.thumb,
                  title: tbPost.title);
              return ValueListenableBuilder(
                  valueListenable: Hive.box<TBPost>(databaseName).listenable(),
                  builder: (context, box, widget1) {
                    //create common widget to display post
                    return WidgetPostDetails(
                      buildContext: context,
                      postDetails: modelPosts,
                      isFavouritePost: true,
                      boxPost: box,
                    );
                  });
            },
            itemCount: boxPost.length,
          );
        });
  }

  Widget widgetAllPosts() {
    return BlocBuilder<PostsCubit, PostsState>(builder: (context, state) {
      if (state is PostsLoading && state.isFirstFetch) {
        return widgetProgressIndicator();
      }
      List<ModelPosts> listPosts = [];
      bool isLoading = false;
      if (state is PostsLoading) {
        listPosts = state.listOldPosts;
        isLoading = true;
      } else if (state is PostsLoaded) {
        listPosts = state.listPosts;
      }

      return PageView.builder(
        controller: pageController,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return ValueListenableBuilder(
              valueListenable: Hive.box<TBPost>(databaseName).listenable(),
              builder: (context, box, widget1) {
                if (index < listPosts.length)
                  return ValueListenableBuilder(
                      valueListenable:
                          Hive.box<TBPost>(databaseName).listenable(),
                      builder: (context, box, widget1) {
                        bool isPostFavourite = false;
                        if (box.containsKey(listPosts[index].title)) {
                          isPostFavourite = true;
                        }
                        return WidgetPostDetails(
                          buildContext: context,
                          postDetails: listPosts[index],
                          isFavouritePost: isPostFavourite,
                          boxPost: box,
                        );
                      });
                else {
                  Timer(Duration(milliseconds: 30), () {
                    scrollController
                        .jumpTo(scrollController.position.maxScrollExtent);
                  });

                  return widgetProgressIndicator();
                }
              });
        },
        itemCount: listPosts.length + (isLoading ? 1 : 0),
      );
    });
  }

//common progress indicator
  Widget widgetProgressIndicator() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CircularProgressIndicator(),
      ),
    );
  }

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<PostsCubit>(context).loadPosts();
        }
      }
    });
  }

//function to open confirmation whether to exit app or not
  Future<bool> exitApp() async {
    HelperFunction.showCommonDialog(context,
        title: 'Confirmation',
        content: Text('Are you sure you want to exit app?'),
        positiveButton: () {
      exit(0);
    });
    return false;
  }
}
