// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TBPost.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TBPostAdapter extends TypeAdapter<TBPost> {
  @override
  final int typeId = 0;

  @override
  TBPost read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TBPost(
      description: fields[1] as String,
      sources: (fields[5] as List)?.cast<String>(),
      subtitle: fields[2] as String,
      thumb: fields[3] as String,
      title: fields[0] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TBPost obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.description)
      ..writeByte(2)
      ..write(obj.subtitle)
      ..writeByte(3)
      ..write(obj.thumb)
      ..writeByte(5)
      ..write(obj.sources);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TBPostAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
