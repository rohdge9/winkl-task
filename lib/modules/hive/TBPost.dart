import 'package:hive/hive.dart';
part 'TBPost.g.dart';

@HiveType(typeId: 0)
class TBPost {
  @HiveField(0)
  final String title;
  @HiveField(1)
  final String description;
  @HiveField(2)
  final String subtitle;
  @HiveField(3)
  final String thumb;
  @HiveField(5)
  final List<String> sources;

  TBPost(
      {this.description, this.sources, this.subtitle, this.thumb, this.title});
}
//add command to generate adapter:
// flutter packages pub run build_runner build
