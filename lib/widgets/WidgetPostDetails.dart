import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:winkl_task/helper/HelperFunction.dart';
import 'package:winkl_task/models/ModelPosts.dart';
import 'package:winkl_task/modules/hive/TBPost.dart';
import 'package:winkl_task/modules/posts/VideoWidget.dart';

class WidgetPostDetails extends StatefulWidget {
  final ModelPosts postDetails;
  final BuildContext buildContext;
  final bool isFavouritePost;
  final Box<TBPost> boxPost;
  WidgetPostDetails(
      {@required this.buildContext,
      @required this.postDetails,
      @required this.isFavouritePost,
      @required this.boxPost});

  @override
  _WidgetPostDetailsState createState() => _WidgetPostDetailsState();
}

class _WidgetPostDetailsState extends State<WidgetPostDetails> {
  List<Widget> listVideoSliders = [];
  final CarouselController carouselController = CarouselController();

  @override
  void initState() {
    super.initState();
    //making list to pass to carousel
    listVideoSliders = widget.postDetails.sources
        .map((item) => Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: VideoWidget(url: item, play: true)),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${widget.postDetails.title}',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              InkWell(
                onTap: () {
                  if (widget.isFavouritePost) {
                    //remove from favourite
                    widget.boxPost.delete(widget.postDetails.title);
                    HelperFunction.showFlushbarError(context,
                        "Post is successfully removed from favourites!!!");
                  } else {
                    TBPost tbPost = TBPost(
                        title: widget.postDetails.title,
                        description: widget.postDetails.description,
                        sources: widget.postDetails.sources,
                        subtitle: widget.postDetails.subtitle,
                        thumb: widget.postDetails.thumb);
                    //add to favourite
                    widget.boxPost.put("${widget.postDetails.title}", tbPost);
                    HelperFunction.showFlushbarSuccess(
                        context, "Post is successfully added to favourites!!!");
                  }
                },
                child: Icon(
                  widget.isFavouritePost
                      ? Icons.favorite
                      : Icons.favorite_outline,
                  color: widget.isFavouritePost ? Colors.pink : Colors.grey,
                ),
              )
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: CarouselSlider(
              carouselController: carouselController,
              options: CarouselOptions(
                // autoPlay: true,
                aspectRatio: 1.5,
                viewportFraction: 3.0,
                enableInfiniteScroll: false,
                enlargeCenterPage: true,
              ),
              items: listVideoSliders,
            ),
          ),
          Row(
            children: [
              Text(
                'Subtitle : ',
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.grey),
              ),
              Text(
                '${widget.postDetails.subtitle}',
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.blue),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Text(
                  'Videos count : ',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.grey),
                ),
                Text(
                  '${widget.postDetails.sources.length}',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.green),
                ),
              ],
            ),
          ),
          Text(
            'Description : ',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Flexible(
            child: Text(
              '${widget.postDetails.description}',
              maxLines: 9,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
