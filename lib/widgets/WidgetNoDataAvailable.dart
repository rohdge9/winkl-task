import 'package:flutter/material.dart';

class WidgetNoDataAvailable extends StatelessWidget {
  final String title;
  WidgetNoDataAvailable({@required this.title});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/images/noDataFound.png',
          height: 150,
          width: 150,
          fit: BoxFit.contain,
        ),
        Text('$title'),
      ],
    );
  }
}
