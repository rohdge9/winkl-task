import 'package:flutter/material.dart';

class WidgetAppBar extends StatefulWidget with PreferredSizeWidget {
  final String title;
  final List<Widget> listWidgets;
  WidgetAppBar({@required this.title, this.listWidgets});
  @override
  _WidgetAppBarState createState() => _WidgetAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _WidgetAppBarState extends State<WidgetAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      titleSpacing: 0.0,
      elevation: 0.5,
      centerTitle: true,
      backgroundColor: Colors.white,
      title: Text(
        '${widget.title}',
        style: TextStyle(
            color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 18),
      ),
      actions: widget.listWidgets != null ? widget.listWidgets : [],
    );
  }
}
