// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ModelPosts convertFromJson(String str) => ModelPosts.fromJson(json.decode(str));

String convertToJson(ModelPosts data) => json.encode(data.toJson());

class ModelPosts {
  ModelPosts({
    this.description,
    this.sources,
    this.subtitle,
    this.thumb,
    this.title,
  });

  String description;
  List<String> sources;
  String subtitle;
  String thumb;
  String title;

  factory ModelPosts.fromJson(Map<String, dynamic> json) => ModelPosts(
        description: json["description"],
        sources: List<String>.from(json["sources"].map((x) => x)),
        subtitle: json["subtitle"],
        thumb: json["thumb"],
        title: json["title"],
      );

  Map<String, dynamic> toJson() => {
        "description": description,
        "sources": List<dynamic>.from(sources.map((x) => x)),
        "subtitle": subtitle,
        "thumb": thumb,
        "title": title,
      };
}
