import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:winkl_task/bloc/cubit/posts_cubit.dart';
import 'package:winkl_task/modules/hive/TBPost.dart';
import 'package:winkl_task/modules/posts/DisplayPost.dart';
import 'package:winkl_task/repositories/RepositoryPosts.dart';
import 'package:winkl_task/services/ServicePosts.dart';

const databaseName = "winklDB";
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialiseDB();
  runApp(MyApp(
    repositoryPosts: RepositoryPosts(ServicePosts()),
  ));
}

initialiseDB() async {
  final document = await getApplicationDocumentsDirectory();
  Hive.init(document.path);
  Hive.registerAdapter(TBPostAdapter());
  await Hive.openBox<TBPost>(databaseName);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final RepositoryPosts repositoryPosts;

  const MyApp({Key key, this.repositoryPosts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) => PostsCubit(repositoryPosts),
        child: DisplayPost(),
      ),
    );
  }
}
